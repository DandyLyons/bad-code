// This code has a logic error.
// It doesn't produce a syntax or runtime error message.
// See if you can figure it out.
const user = {
    name: "John",
    role: "admin",
    isActive: false
};

let canAccess;

if (user.role === "admin" && user.isActive) {
    canAccess = true;
} else {
    canAccess = false;
}

console.log(user.name + " can access the admin interface: " + (canAccess ? "Yes" : "No")); // Expected: Yes
