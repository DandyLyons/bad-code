// This problem has a logic error.
// It doesn't produce a syntax error or a runtime error
// See if you can figure out what's wrong.
const players = ["John", "Sarah", "Alex", "Mary"];
// const currentPlayer = players[4]; Error index 4 is called but there are only 4 items
const currentPlayer = players[3]

console.log("The current player is " + currentPlayer);
