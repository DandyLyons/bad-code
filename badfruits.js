// This file has a logic error.
// It doesn't produce a syntax error or a runtime error
// See if you can figure out what's wrong

const fruits = ["apple", "banana", "orange", "mango"];
const myFruit = "apple";

const hasFruit = fruits[0];

if (hasFruit !== undefined) {
    console.log("I have an " + myFruit);
} else {
    console.log("I don't have an " + myFruit);
}
