// This code has a logic error.
// It doesn't produce a syntax error or a runtime error
// See if you can find out why it's happening and fix it to get the right answer
const weather = "rainy";
let umbrella = false;

if (weather === "rainy") {
    umbrella = true;
}

console.log("Do I need an umbrella? " + (umbrella ? "Yes" : "No")); // Expected: Yes for rainy weather
