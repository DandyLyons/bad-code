const speed = 70;
const speedLimit = 60;

let isSpeeding;

if (speed == speedLimit) {
    isSpeeding = false;
} else {
    isSpeeding = true;
}

console.log("Is the vehicle speeding? " + (isSpeeding ? "Yes" : "No")); // Expected: Yes
